# NinCommands [![](https://jitpack.io/v/com.gitlab.martijn-heil/NinCommands.svg)](https://jitpack.io/#com.gitlab.martijn-heil/NinCommands)
An adaptation of [Intake](https://github.com/EngineHub/Intake) for Bukkit.

A basic example (written in [Kotlin](https://kotlinlang.org/)):
```kotlin
import com.gitlab.martijn_heil.nincommands.common.CommonModule
import com.gitlab.martijn_heil.nincommands.common.CommandTarget
import com.gitlab.martijn_heil.nincommands.common.Toggle
import com.gitlab.martijn_heil.nincommands.common.bukkit.BukkitAuthorizer
import com.gitlab.martijn_heil.nincommands.common.bukkit.provider.BukkitModule
import com.gitlab.martijn_heil.nincommands.common.bukkit.provider.sender.BukkitSenderModule
import com.gitlab.martijn_heil.nincommands.common.bukkit.registerCommand
import com.sk89q.intake.Intake
import com.sk89q.intake.fluent.CommandGraph
import com.sk89q.intake.parametric.ParametricBuilder
import com.sk89q.intake.parametric.provider.PrimitivesModule
import com.sk89q.intake.Command
import com.sk89q.intake.Require
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin

class MyPlugin : JavaPlugin() {
    override fun onEnable() {
        val injector = Intake.createInjector()
        injector.install(PrimitivesModule())
        injector.install(BukkitModule(server))
        injector.install(BukkitSenderModule())
        injector.install(CommonModule())

        val builder = ParametricBuilder(injector)
        builder.authorizer = BukkitAuthorizer()

        val dispatcher = CommandGraph()
                .builder(builder)
                .commands()
                .registerMethods(MyCommands())
                .graph()
                .dispatcher

        registerCommand(dispatcher, this, dispatcher.aliases.toList())
    }
}

class MyCommands {
    @Command(aliases = ["mycommand", "mc"], desc = "Toggle my command")
    @Require("mypermission")
    fun mycommand(@Toggle value: Boolean, @CommandTarget("mycommand.others") target: Player) {
        // value is a boolean, but instead of the usual true/false,
        // it is called using on/off in the command, by the @Toggle
        // so you call this command like `mycommand on` or `mycommand off`
        //
        // The @CommandTarget target: Player with the associated permission will
        // be the command sender himself, or optionally, if the command sender has the
        // associated permission *and* has specified another player,
        // the specified player. Like as follows:
        // `mycommand on SomePlayername`, `mycommand off SomePlayername`
        //
        // for more in-depth explanation and examples of how to use Intake in
        // general, of course, see the Intake documentation.
        // ...
    }
}
```