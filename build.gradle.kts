import com.github.jengelman.gradle.plugins.shadow.ShadowExtension
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.JavaVersion
import java.net.URI
import org.apache.tools.ant.filters.*
import org.gradle.plugins.ide.idea.model.IdeaLanguageLevel

plugins {
    `java-gradle-plugin`
    `java-library`
    kotlin("jvm") version "1.8.10"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    idea
    `maven-publish`
}

group = "com.gitlab.martijn-heil"
version = "master-SNAPSHOT"
description = "NinCommands"

apply {
    plugin("java")
    plugin("java-library")
    plugin("kotlin")
    plugin("com.github.johnrengelman.shadow")
    plugin("idea")
    plugin("maven-publish")
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

kotlin {
    jvmToolchain(17)
}

tasks {
    withType<ProcessResources> {
        filter(mapOf(Pair("tokens", mapOf(Pair("version", version)))), ReplaceTokens::class.java)
    }
    withType<ShadowJar> {
        //this.classifier = null
        this.configurations = listOf(project.configurations.shadow.get())
    }
}

tasks["build"].dependsOn("shadowJar")

defaultTasks = mutableListOf("build")

repositories {
    maven { url = URI("https://hub.spigotmc.org/nexus/content/repositories/snapshots/") }
    maven { url = URI("https://jitpack.io") }
    mavenCentral()
    mavenLocal()
}

idea {
    project {
        languageLevel = IdeaLanguageLevel("17")
    }
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
    }
}

dependencies {
    compileOnly("org.spigotmc:spigot-api:1.19.2-R0.1-SNAPSHOT") { isChanging = true }
    compileOnly(fileTree("lib") { include("*.jar") })
    shadow(kotlin("stdlib"))
    shadow("com.gitlab.martijn-heil:Intake:63c036ab24")
    api("com.gitlab.martijn-heil:Intake:63c036ab24")
}
