package com.gitlab.martijn_heil.nincommands.common

import com.sk89q.intake.parametric.annotation.Classifier

@Classifier
@Retention
annotation class FixedSuggestions
