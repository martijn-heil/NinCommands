package com.gitlab.martijn_heil.nincommands.common

import com.gitlab.martijn_heil.nincommands.common.provider.FixedSuggestionsProvider
import com.sk89q.intake.parametric.AbstractModule
import com.sk89q.intake.parametric.Injector

class FixedSuggestionsModule(private val injector: Injector) : AbstractModule() {
    override fun configure() {
        bind(Boolean::class.java).annotatedWith(FixedSuggestions::class.java).toProvider(FixedSuggestionsProvider(injector.getProvider(Boolean::class.java)!!))
        bind(Int::class.java).annotatedWith(FixedSuggestions::class.java).toProvider(FixedSuggestionsProvider(injector.getProvider(Int::class.java)!!))
        bind(Short::class.java).annotatedWith(FixedSuggestions::class.java).toProvider(FixedSuggestionsProvider(injector.getProvider(Short::class.java)!!))
        bind(Double::class.java).annotatedWith(FixedSuggestions::class.java).toProvider(FixedSuggestionsProvider(injector.getProvider(Double::class.java)!!))
        bind(Float::class.java).annotatedWith(FixedSuggestions::class.java).toProvider(FixedSuggestionsProvider(injector.getProvider(Float::class.java)!!))
        bind(String::class.java).annotatedWith(FixedSuggestions::class.java).toProvider(FixedSuggestionsProvider(injector.getProvider(String::class.java)!!))
    }
}
