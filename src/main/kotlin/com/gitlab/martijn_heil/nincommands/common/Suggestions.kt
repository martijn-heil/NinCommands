package com.gitlab.martijn_heil.nincommands.common

@Retention
annotation class Suggestions(val suggestions: Array<String>)
