/*
 *
 * NinCommands
 * Copyright (C) 2018-2022 Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.martijn_heil.nincommands.common.bukkit


import com.sk89q.intake.argument.Namespace
import com.sk89q.intake.util.auth.Authorizer
import org.bukkit.permissions.Permissible

class BukkitAuthorizer : Authorizer {

    override fun testPermission(namespace: Namespace, perm: String): Boolean {
        val permissible = namespace.get(Permissible::class.java)
        return permissible != null && permissible.hasPermission(perm)
    }
}
