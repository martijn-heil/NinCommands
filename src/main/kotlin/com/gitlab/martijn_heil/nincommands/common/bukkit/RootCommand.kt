/*
 *
 * NinCommands
 * Copyright (C) 2018-2022 Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.martijn_heil.nincommands.common.bukkit


import com.sk89q.intake.CommandCallable
import com.sk89q.intake.CommandException
import com.sk89q.intake.InvocationCommandException
import com.sk89q.intake.argument.Namespace
import com.sk89q.intake.dispatcher.Dispatcher
import com.sk89q.intake.parametric.ProvisionException
import com.sk89q.intake.util.auth.AuthorizationException
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.command.CommandSender
import org.bukkit.command.defaults.BukkitCommand
import org.bukkit.entity.Entity
import org.bukkit.entity.Player
import org.bukkit.permissions.Permissible

class RootCommand(private val callable: CommandCallable, aliases: List<String>, private val fallbackPrefix: String) :
        BukkitCommand(aliases[0], callable.description.shortDescription ?: "", callable.description.usage, aliases) {

    init {
        this.description = callable.description.shortDescription ?: ""
    }


    override fun execute(sender: CommandSender, cmd: String, args: Array<String>): Boolean {
        val namespace = constructNamespace(sender, null)
        val arguments = args.joinToString(" ")

        try {
            callable.call(arguments, namespace, emptyList<String>())
        } catch (cx: CommandException) {
            sendError(sender, cx.message ?: "Unknown error.")
        } catch (cx: AuthorizationException) {
            sendError(sender, cx.message ?: "Permission denied.")
        } catch (icx: InvocationCommandException) {
            when (icx.cause) {
                is AuthorizationException -> { sendError(sender, icx.cause!!.message ?: "Permission denied.") }
                is CommandException -> { sendError(sender, icx.cause!!.message ?: "Unknown error.") }
                is ProvisionException -> { sendError(sender, icx.cause!!.message ?: "Unknown error") }
                else -> {
                    sendError(sender, "An internal server error occurred")
                    icx.printStackTrace()
                }
            }
        } catch(ex: Exception) {
            sendError(sender, "An internal server error occurred")
            ex.printStackTrace()
            throw ex
        }

        return true
    }

    override fun tabComplete(sender: CommandSender, alias: String, args: Array<String>, location: Location?): List<String> {
        return callable.getSuggestions(args.joinToString(" "), constructNamespace(sender, location))
    }

    private fun sendError(p: CommandSender, message: String) {
        p.sendMessage(ChatColor.RED.toString() + "Error: " + ChatColor.DARK_RED + message)
    }

    private fun constructNamespace(sender: CommandSender, location: Location?): Namespace {
        var loc: Location? = location
        if (loc == null && sender is Entity) loc = sender.location

        val namespace = Namespace()
        namespace.put("sender", sender)
        namespace.put("senderClass", sender.javaClass)
        namespace.put(Permissible::class.java, sender)
        namespace.put("senderLocation", loc)

        return namespace
    }
}
