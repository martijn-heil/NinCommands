/*
 *
 * NinCommands
 * Copyright (C) 2018-2022 Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.martijn_heil.nincommands.common.bukkit.provider


import com.sk89q.intake.argument.ArgumentException
import com.sk89q.intake.argument.CommandArgs
import com.sk89q.intake.argument.Namespace
import com.sk89q.intake.parametric.Provider
import com.sk89q.intake.parametric.ProvisionException
import org.bukkit.Material

class MaterialProvider() : Provider<Material> {

    override fun isProvided() = false


    @Throws(ArgumentException::class, ProvisionException::class)
    override fun get(commandArgs: CommandArgs, modifiers: List<Annotation>): Material? {
        return Material.matchMaterial(commandArgs.next())
    }

    override fun getSuggestions(prefix: String, locals: Namespace, modifiers: List<Annotation>): List<String> {
        return Material.values()
            .map { it.name.lowercase() }
            .plus(Material.values().map { "minecraft:${it.name.lowercase()}" })
            .filter { it.startsWith(prefix) }
    }
}
