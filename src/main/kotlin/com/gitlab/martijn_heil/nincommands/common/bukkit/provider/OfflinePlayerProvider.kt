/*
 *
 * NinCommands
 * Copyright (C) 2018-2022 Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.martijn_heil.nincommands.common.bukkit.provider


import com.sk89q.intake.argument.ArgumentException
import com.sk89q.intake.argument.ArgumentParseException
import com.sk89q.intake.argument.CommandArgs
import com.sk89q.intake.parametric.Provider
import com.sk89q.intake.parametric.ProvisionException
import com.gitlab.martijn_heil.nincommands.common.CommandTarget
import com.sk89q.intake.argument.Namespace
import org.bukkit.OfflinePlayer
import org.bukkit.Server
import org.bukkit.command.CommandSender
import org.bukkit.permissions.Permissible
import java.util.UUID

class OfflinePlayerProvider(private val server: Server) : Provider<OfflinePlayer> {

    override fun isProvided() = false


    @Throws(ArgumentException::class, ProvisionException::class)
    override fun get(commandArgs: CommandArgs, modifiers: List<Annotation>): OfflinePlayer? {
        val sender = commandArgs.namespace.get("sender") as CommandSender
        var targetAnnotation: CommandTarget? = null

        for (annotation in modifiers) {
            if (annotation is CommandTarget) {
                targetAnnotation = annotation
                break
            }
        }

        var p: OfflinePlayer? = null

        if (commandArgs.hasNext()) {
            val arg = commandArgs.next()

            if (arg.matches("[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}".toRegex()))
            // it's a UUID.
            {
                p = server.getOfflinePlayer(UUID.fromString(arg))
            } else {
                // Given deprecation only serves as a warning about UUID's,
                // it is not scheduled for removal or anything
                // We are very aware of why the deprecation is here, so it's not a problem
                @Suppress("DEPRECATION")
                p = server.getOfflinePlayer(arg)
            }
        } else if (targetAnnotation != null && sender is OfflinePlayer) {
            p = sender
        } else {
            // Generate MissingArgumentException
            commandArgs.next()
        }

        if (p == null || !p.hasPlayedBefore()) {
            throw ArgumentParseException("Player not found")
        }

        if (targetAnnotation != null &&
                p != sender &&
                !commandArgs.namespace.get(Permissible::class.java)!!.hasPermission(targetAnnotation.value)) {
            throw ArgumentParseException("You need " + targetAnnotation.value)
        }

        return p
    }


    override fun getSuggestions(prefix: String, locals: Namespace, modifiers: List<Annotation>): List<String> {
        return server.onlinePlayers.map { it.name }.filter { it.lowercase().startsWith(prefix.lowercase()) }
    }
}
