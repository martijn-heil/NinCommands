/*
 *
 * NinCommands
 * Copyright (C) 2018-2022 Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.martijn_heil.nincommands.common.bukkit.provider


import com.sk89q.intake.argument.ArgumentException
import com.sk89q.intake.argument.ArgumentParseException
import com.sk89q.intake.argument.CommandArgs
import com.sk89q.intake.argument.Namespace
import com.sk89q.intake.parametric.Provider
import com.sk89q.intake.parametric.ProvisionException
import org.bukkit.OfflinePlayer
import org.bukkit.Server
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class PlayerProvider(private val server: Server, private val offlinePlayerProvider: Provider<OfflinePlayer>) : Provider<Player> {

    override fun isProvided() = false


    @Throws(ArgumentException::class, ProvisionException::class)
    override fun get(commandArgs: CommandArgs, modifiers: List<Annotation>): Player? {
        val sender = commandArgs.namespace.get("sender") as CommandSender

        // Vanilla command target selector
        if (commandArgs.hasNext() && commandArgs.peek().startsWith("@")) {
            try {
                return server.selectEntities(sender, commandArgs.next()).filterIsInstance<Player>().firstOrNull()
                    ?: throw ArgumentParseException("No player found")
            } catch (ex: IllegalArgumentException) { throw ArgumentParseException(ex.message) }
        }

        val op = offlinePlayerProvider.get(commandArgs, modifiers)
        if (op != null) {
            if (!op.isOnline) throw ArgumentParseException("Player not found")

            return op.player
        } else {
            throw ArgumentParseException("Player not found")
        }
    }

    override fun getSuggestions(prefix: String, locals: Namespace, modifiers: List<Annotation>): List<String> {
        // TODO UUID suggestions?
        val matches = server.matchPlayer(prefix)
        val suggestions = ArrayList<String>()
        matches.forEach { match -> suggestions.add(match.name) }
        return suggestions
    }
}
