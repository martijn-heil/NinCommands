/*
 *
 * NinCommands
 * Copyright (C) 2018-2022 Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.martijn_heil.nincommands.common.bukkit.provider.sender


import com.sk89q.intake.argument.ArgumentException
import com.sk89q.intake.argument.CommandArgs
import com.sk89q.intake.argument.Namespace
import com.sk89q.intake.parametric.Provider
import com.sk89q.intake.parametric.ProvisionException
import org.bukkit.command.CommandSender

class BukkitSenderProvider<T: CommandSender>(val clazz: Class<T>) : Provider<T> {

    override fun isProvided() = true

    @Throws(ArgumentException::class, ProvisionException::class)
    override fun get(commandArgs: CommandArgs, list: List<Annotation>): T? {
        val namespace = commandArgs.namespace
        val senderClass = namespace.get("senderClass") as? Class<*>
                ?: throw ProvisionException("Sender's class was not set on namespace")

        if (!namespace.containsKey("sender")) {
            throw ProvisionException("Sender was not set on namespace")
        }

        val rawSender = namespace.get("sender")
        // ! rawSender instanceof clazz
        if (!clazz.isInstance(rawSender)) {
            throw InvalidSenderTypeException(senderClass.simpleName, clazz.simpleName)
        }

        val sender = namespace.get("sender") as T? ?:
            throw ProvisionException("Sender was set on namespace, but is null")

        return sender
    }


    override fun getSuggestions(prefix: String, locals: Namespace, modifiers: List<Annotation>): List<String> {
        return emptyList()
    }
}
