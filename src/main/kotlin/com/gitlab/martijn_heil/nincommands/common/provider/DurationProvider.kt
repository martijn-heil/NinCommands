package com.gitlab.martijn_heil.nincommands.common.provider

import com.sk89q.intake.argument.ArgumentException
import com.sk89q.intake.argument.CommandArgs
import com.sk89q.intake.argument.Namespace
import com.sk89q.intake.parametric.Provider
import com.sk89q.intake.parametric.ProvisionException
import java.time.Duration
import kotlin.time.toJavaDuration

class DurationProvider : Provider<Duration> {
	override fun isProvided() = false

	@Throws(ArgumentException::class, ProvisionException::class)
	@OptIn(kotlin.time.ExperimentalTime::class)
	override fun get(arguments: CommandArgs, modifiers: List<Annotation>)
		= kotlin.time.Duration.parse(arguments.next()).toJavaDuration()

	override fun getSuggestions(prefix: String, locals: Namespace, modifiers: List<Annotation>): List<String>
		= emptyList()
}
