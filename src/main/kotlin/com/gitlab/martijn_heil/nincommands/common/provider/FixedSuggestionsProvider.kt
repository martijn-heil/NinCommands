package com.gitlab.martijn_heil.nincommands.common.provider

import com.gitlab.martijn_heil.nincommands.common.Suggestions
import com.sk89q.intake.argument.CommandArgs
import com.sk89q.intake.argument.Namespace
import com.sk89q.intake.parametric.Provider

class FixedSuggestionsProvider<T>(private val parent: Provider<T>) : Provider<T> {
	override fun isProvided() = parent.isProvided

	override fun get(arguments: CommandArgs?, modifiers: MutableList<out Annotation>?) =
			parent.get(arguments, modifiers)

	override fun getSuggestions(prefix: String?, locals: Namespace?, modifiers: MutableList<out Annotation>): MutableList<String> =
			modifiers.find { it is Suggestions }
					?.let { it as Suggestions }?.suggestions?.toMutableList() ?: mutableListOf()
}
